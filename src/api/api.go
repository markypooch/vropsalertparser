package api

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"encoding/xml"
	"errors"
	"time"
	"fmt"
	"io/ioutil"
	"net/http"
	vrops "types/v1"
)

type VropsSession struct {
	Url        string
	VropsToken string `json:"token"`
	Validity   int64  `json:"validity"`
	ExpiresAt  string `json:"expiresAt"`
}

func NewSession(url string, username string, password string) (*VropsSession, error) {

	var returnerr error
	vropsTokenAquire := vrops.VropsTokenAcquire{
		Url:      url,
		Username: username,
		Password: password,
		Xs:       "http://www.w3.org/2001/XMLSchema",
		Xsi:      "http://www.w3.org/2001/XMLSchema-instance",
		Ops:      "http://webservice.vmware.com/vRealizeOpsMgr/1.0/",
	}
	vropsSession, err := authenticate(vropsTokenAquire)
	if err != nil {
		returnerr = err
	}

	return vropsSession, returnerr
}

func (session *VropsSession) CloseSession(Url string, Username string, Password string) {
	
	err := session.Do("/suite-api/api/auth/token/release", "POST", nil, nil)
	if err != nil {
		panic(err)
	}
	
}

func authenticate(vropsTokenAcquire vrops.VropsTokenAcquire) (*VropsSession, error) {
	
	var vropsSession *VropsSession
	var returnerror error

	xmlBytes, err := xml.Marshal(vropsTokenAcquire)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(xmlBytes))

	request, client := buildVropsRequest("POST", vropsTokenAcquire.Url, "/suite-api/api/auth/token/acquire", xmlBytes)
	request.SetBasicAuth(vropsTokenAcquire.Username, vropsTokenAcquire.Password)

	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}

	if response.StatusCode < 200 || response.StatusCode > 300 {
	    body, _ := ioutil.ReadAll(response.Body)
		returnerror = errors.New("Non 200 status code returned in authentication: " + string(response.StatusCode) + " : " + string(body))
	} else {
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			panic(err)
		}

		err = json.Unmarshal(body, &vropsSession)
		if err != nil {
			panic(err)
		}
		vropsSession.Url = vropsTokenAcquire.Url
	}

	return vropsSession, returnerror
}

func buildVropsRequest(method string, url string, endpoint string, body []byte) (*http.Request, *http.Client) {
    defaultTransport := http.DefaultTransport.(*http.Transport)
	httpClientWithSelfSignedTLS := &http.Transport{
        Proxy:                 defaultTransport.Proxy,
        DialContext:           defaultTransport.DialContext,
        MaxIdleConns:          defaultTransport.MaxIdleConns,
        IdleConnTimeout:       time.Second * 270,
        ExpectContinueTimeout: time.Second * 270,
        TLSHandshakeTimeout:   time.Second * 270,
        TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
    }
    
	request, err := http.NewRequest(method, url+endpoint, bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}

	request.Header.Set("Content-type", "text/xml")
	request.Header.Set("accept", "application/json")

	client := &http.Client{
	    Timeout: time.Second * 270,
        Transport: httpClientWithSelfSignedTLS,
	}

	return request, client
}

func (session *VropsSession) Do(endpoint string, method string, requestType interface{}, responseType interface{}) error {

	var returnerror error
	var xmlBytes []byte

	if requestType != nil {

		xmlBytes, returnerror = xml.Marshal(requestType)
		if returnerror != nil {
			panic(returnerror)
		}
	}

	request, client := buildVropsRequest(method, session.Url, endpoint, xmlBytes)
	request.Header.Set("Authorization", "vRealizeOpsToken "+session.VropsToken)
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}

	if response.StatusCode < 200 || response.StatusCode > 300 {
	    body, _ := ioutil.ReadAll(response.Body)
		returnerror = errors.New("Non 200 status code returned in authentication: " + string(response.StatusCode) + " : " + string(body))
	} else {
	    fmt.Println("Call to " + endpoint + " succeeded")
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			panic(err)
		}

		if responseType != nil {
			err = json.Unmarshal(body, &responseType)
			if err != nil {
				panic(err)
			}
		}
	}

	return returnerror
}
