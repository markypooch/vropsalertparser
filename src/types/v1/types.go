package vrops

import "encoding/xml"

type VropsTokenAcquire struct {
	XMLName  xml.Name `xml:"ops:username-password"`
	Text     string   `xml:",chardata"`
	Ops      string   `xml:"xmlns:ops,attr"`
	Xs       string   `xml:"xmlns:xs,attr"`
	Xsi      string   `xml:"xmlns:xsi,attr"`
	Username string   `xml:"ops:username"`
	//AuthSource string   `xml:"ops:authSource",omitempty`
	Password string `xml:"ops:password"`
	Url      string
}
type VropsStartTimeRange struct {
	StartTime int64 `xml:"ops:startTime,omitempty"` // 0
	EndTime   int64 `xml:"ops:endTime,omitempty"`   // filterTime
}
type VropsAlertQuery struct {
	XMLName                  xml.Name             `xml:"ops:alert-query,omitempty"`
	CompositeOperator        string               `xml:"compositeOperator,attr"`
	Xs                       string               `xml:"xmlns:xs,attr"`
	Xsi                      string               `xml:"xmlns:xsi,attr"`
	Ops                      string               `xml:"xmlns:ops,attr"`
	ActiveOnly               bool                 `xml:"ops:activeOnly,omitempty"` // true
	AlertId                  string               `xml:"ops:alertId,omitempty"`
	AlertCriticality         string               `xml:"ops:alertCriticality,omitempty"`         // CRITICAL IMMEDIATE
	IncludeChildrenResources bool                 `xml:"ops:includeChildrenResources,omitempty"` // true
	StartTimeRange           *VropsStartTimeRange `xml:"ops:startTimeRange,omitempty"`
}

type VropsProblemDescription struct {
	Description string `json:"description"`
	Name        string `json:"name"`
}
type VropsResourceKey struct {
	Name            string `json:"name"`
	ResourceKindKey string `json:"resourceKindKey"`
	AdapterKindKey  string `json:"adapterKindKey"`
}
type VropsAlertsResource struct {
	ResourceKey VropsResourceKey `json:"resourceKey"`
}
type VropsLinks struct {
	Description string `json:"description"`
	Href        string `json:"href"`
	Name        string `json:"name"`
	Rel         string `json:"rel"`
}
type VropsAlerts struct {
	AlertId             string       `json:"alertId"`
	ResourceId          string       `json:"resourceId"`
	AlertLevel          string       `json:"alertLevel"`
	Type                string       `json:"type"`
	SubType             string       `json:"subType"`
	Status              string       `json:"status"`
	StartTimeUTC        int          `json:"startTimeUTC"`
	CancelTimeUTC       int          `json:"cancelTimeUTC"`
	UpdateTimeUTC       int          `json:"updateTimeUTC"`
	SuspendUntilTimeUTC int          `json:"suspendUntilTimeUTC"`
	ControlState        string       `json:"controlState"`
	StatKey             string       `json:"statKey"`
	OwnerId             string       `json:"ownerId"`
	OwnerName           string       `json:"ownerName"`
	AlertDefinitionId   string       `json:"alertDefinitionId"`
	AlertDefinitionName string       `json:"alertDefinitionName"`
	AlertImpact         string       `json:"alertImpact"`
	Others              []string     `json:"others"`
	OthersAttributes    []string     `json:"otherAttributes"`
	Links               []VropsLinks `json:"links"`
}
type VropsAlertQueryResponse struct {
	Alerts []VropsAlerts `json:"alerts"`
}
type VropsVersion struct {
	ReleaseName                string `json:"releaseName"`
	Major                      int16  `json:"major"`
	Minor                      int16  `json:"minor"`
	MinorMinor                 int16  `json:"minorMinor"`
	BuildNumber                int16  `json:"buildNumber"`
	HumanlyReadableReleaseDate string `json:"humanlyReadableReleaseDate"`
	Description                string `json:"description"`
}
type VropsVersionCollection struct {
	Versions []VropsVersion `json:"values"`
}
