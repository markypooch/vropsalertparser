package main

import (
	"api"
	"database/sql"
	"fmt"
	"os"
	"testing"
	vrops "types/v1"

	_ "github.com/go-sql-driver/mysql"
)

var session *api.VropsSession
var dbAlerts []Alert
var db *sql.DB
var alertStringIds string

func recoverPanic(T *testing.T) {
	if r := recover(); r != nil {
		fmt.Println("recovered from ", r)
	}
}

func Test_retrieveAlertCriticality(T *testing.T) {

	var crit int
	crit = retrieveAlertCriticality("CRITICAL")
	if crit != 1 {
		T.Error("Critical string did not return proper error number (1), returned: ", crit)
	}
	crit = retrieveAlertCriticality("WARNING")
	if crit != 4 {
		T.Error("Warning string did not return proper error number (4), returned: ", crit)
	}
	crit = retrieveAlertCriticality("NONSENSE")
	if crit != 5 {
		T.Error("Nonsense string did not return proper error number (5), returned: ", crit)
	}
}

func Test_addNewAlertToSlice(T *testing.T) {

	if len(dbAlerts) == 0 {
		Test_retrieveDBAlerts(T)
	}

	var newAlert []Alert
	newAlert = append(newAlert, dbAlerts[len(dbAlerts)-1])
	newAlert[0].AlertId += "9a9e"

	newAlerts := addToSliceIfNewAlert(dbAlerts, newAlert)
	if len(newAlerts) < 96 {
		T.Error("Error, expected 96, instead got: ", len(newAlerts))
	}
}

func Test_addNewAlertToSliceWithNoNewAlerts(T *testing.T) {

	if len(dbAlerts) == 0 {
		Test_retrieveDBAlerts(T)
	}
	var newAlert []Alert
	newAlerts := addToSliceIfNewAlert(dbAlerts, newAlert)
	if len(newAlerts) < 95 {
		T.Error("Error, expected 95, instead got: ", len(newAlerts))
	}
}

func Test_retrieveAlertsWithNilSession(T *testing.T) {
	defer recoverPanic(T)
	//Issue query, and retrieve results
	vropsDBAlertQuery := vrops.VropsAlertQuery{
		Xs:                "http://www.w3.org/2001/XMLSchema",
		Xsi:               "http://www.w3.org/2001/XMLSchema-instance",
		Ops:               "http://webservice.vmware.com/vRealizeOpsMgr/1.0/",
		CompositeOperator: "AND",
		AlertId:           "XXXXXXXX",
	}
	var vropsAlertQueryDBResponse vrops.VropsAlertQueryResponse
	retrieveAlerts(nil, vropsDBAlertQuery, vropsAlertQueryDBResponse)
}

func Test_retrieveDBAlerts(T *testing.T) {

	if len(dbAlerts) == 0 {

		//Grab all alerts whose monitoringSystem column equals "VROPS"
		rows, err := db.Query("SELECT alertId FROM " + os.Getenv("DB_NAME") + "." + os.Getenv("DB_TABLE") + " WHERE monitoringSystem='VROPS'")
		if err != nil {
			panic(err)
		}

		//Prepare our first VROPs alerts query, search for all alertIDs that we retrieved from the DB
		for rows.Next() {
			var alertId string
			rows.Scan(&alertId)

			//Create a space separated string to hold our alertIDs
			alertStringIds += " " + string(alertId) + " "
		}

		fmt.Println("Retrieving alerts")
		dbAlerts = retrieveDBAlerts(session, alertStringIds)
		if len(dbAlerts) < 95 {
			T.Error("Error, not enough alerts returned from DB, expected 95, got: ", len(dbAlerts))
		}
	}
}

func Test_retrieveDBAlertsWithNullSearchString(T *testing.T) {

	alerts := retrieveDBAlerts(session, "XXXXXXXXXX")
	if len(alerts) != 0 {
		T.Error("Error, alert returned for bogus search string", len(alerts))
	}
}

func TestMain(M *testing.M) {
	//Open our mysql db connection, and defer it's close
	var err error
	db, err = sql.Open("mysql", os.Getenv("DB_USERNAME")+":"+os.Getenv("DB_PASSWORD")+"@tcp("+os.Getenv("DB_IP")+":"+os.Getenv("DB_PORT")+")/"+"")
	if err != nil {
		panic(err)
	}

	session, err = api.NewSession("https://"+os.Getenv("VROPS_URL"), os.Getenv("VROPS_USERNAME"), os.Getenv("VROPS_PASSWORD"))

	errorCode := M.Run()

	err = db.Close()
	if err != nil {
		panic(err)
	}

	session.CloseSession()
	os.Exit(errorCode)
}
