package main

import (
	"api"
	"database/sql"
	"flag"
	"fmt"
	"math"
	"time"
	"strings"
	vrops "types/v1"

	_ "github.com/go-sql-driver/mysql"
)

type Alert struct {
	AlertId          string `db:"alertId"`
	AlertStatus      string `db:"alertStatus"`
	AlertStartTime   int    `db:"alertStartTime"`
	AlertUpdateTime  int    `db:"alertUpdateTime"`
	AlertCancelTime  int    `db:"alertCancelTime"`
	AlertMessage     string `db:"alertMessage"`
	AlertSubject     string `db:"alertSubject"`
	AlertType        string `db:"alertType"`
	EntityName       string `db:"entityName"`
	EntityType       string `db:"entityType"`
	EntityId         string `db:"entityId"`
	AlertURL         string `db:"alertUrl"`
	AlertCriticality int    `db:"alertCriticality"`
	MonitoringSystem string `db:"monitoringSystem"`
}

func retrieveAlertCriticality(criticality string) int {
	var criticalityNum int
	switch criticality {
	case "CRITICAL":
		criticalityNum = 1
		break
	case "IMMEDIATE":
		criticalityNum = 2
		break
	case "INFO":
		criticalityNum = 3
		break
	case "WARNING":
		criticalityNum = 4
		break
	default:
		criticalityNum = 5
		break
	}
	return criticalityNum
}

func addToSliceIfNewAlert(alertsDB []Alert, newAlerts []Alert) []Alert {

	//Perform a diff of the alertsDB passed in, and the newAlerts.
	//If a newAlerts exists that's not in the alertsDB, make sure
	//it's added to the new slice that's returned.
	var vropsAlerts []Alert
	vropsAlerts = alertsDB

	var newAlertCount int
	for _, newalert := range newAlerts {
		foundAlert := false
		for _, dbalert := range alertsDB {
			if newalert.AlertId == dbalert.AlertId {
				foundAlert = true
				break
			}
		}

		if !foundAlert {
			newAlertCount += 1
			vropsAlerts = append(vropsAlerts, newalert)
		}
	}

	fmt.Println("Number of new alerts found: ", newAlertCount)
	return vropsAlerts
}

func retrieveAlerts(vropsSession *api.VropsSession, vropsAlertQuery vrops.VropsAlertQuery, vropsAlertResponseQuery vrops.VropsAlertQueryResponse, cachedDBAlerts bool) ([]Alert, int) {

	//make a query post for our alerts
	var vropsAlertQueryResponse vrops.VropsAlertQueryResponse
	err := vropsSession.Do("/suite-api/api/alerts/query?pageSize=5000", "POST", vropsAlertQuery, &vropsAlertQueryResponse)
	if err != nil {
		panic(err)
	}

	var alertCanceledNum int

	//loop through each alert, and also grab their problem descriptions, and, if able, their resource descriptions
	var alerts []Alert
	for _, alert := range vropsAlertQueryResponse.Alerts {
	    var vropsProblemDescription vrops.VropsProblemDescription
	    var vropsResourceKey vrops.VropsAlertsResource
	    if !cachedDBAlerts {
    		alertOnResourceHref := alert.Links[1].Href
    		alertProblemDesc := alert.Links[3].Href
    
    		err := vropsSession.Do(alertOnResourceHref, "GET", nil, &vropsResourceKey)
    		if err != nil {
    			panic(err)
    		}
    
    		err = vropsSession.Do(alertProblemDesc, "GET", nil, &vropsProblemDescription)
    		if err != nil {
    			panic(err)
    		}
	    }

		if alert.Status == "CANCELED" {
			alertCanceledNum += 1
		}

		criticalityId := retrieveAlertCriticality(alert.AlertLevel)
		alerts = append(alerts, Alert{
			AlertId:          alert.AlertId,
			AlertStatus:      alert.Status,
			AlertStartTime:   alert.StartTimeUTC / 1000,
			AlertUpdateTime:  alert.UpdateTimeUTC / 1000,
			AlertCancelTime:  alert.CancelTimeUTC / 1000,
			AlertMessage:     vropsProblemDescription.Description,
			AlertSubject:     vropsProblemDescription.Name,
			AlertType:        alert.AlertDefinitionId,
			EntityName:       vropsResourceKey.ResourceKey.Name,
			EntityType:       string(vropsResourceKey.ResourceKey.ResourceKindKey + " : " + vropsResourceKey.ResourceKey.AdapterKindKey + " : " + alert.AlertDefinitionName),
			EntityId:         vropsResourceKey.ResourceKey.AdapterKindKey,
			AlertURL:         "/ui/index.action#/alert/" + alert.AlertId + "/summary",
			AlertCriticality: criticalityId,
			MonitoringSystem: "VROPS",
		})
	}

	return alerts, alertCanceledNum
}

func retrieveDBAlerts(vropsSession *api.VropsSession, alertStringIds string) []Alert {

	//Issue query, and retrieve results
	vropsDBAlertQuery := vrops.VropsAlertQuery{
		Xs:                "http://www.w3.org/2001/XMLSchema",
		Xsi:               "http://www.w3.org/2001/XMLSchema-instance",
		Ops:               "http://webservice.vmware.com/vRealizeOpsMgr/1.0/",
		CompositeOperator: "AND",
		AlertId:           alertStringIds,
	}
	var vropsAlertQueryDBResponse vrops.VropsAlertQueryResponse
	alertsDB, alertCancelNum := retrieveAlerts(vropsSession, vropsDBAlertQuery, vropsAlertQueryDBResponse, true)
	fmt.Println("Number of alerts retrieved from DB: ", len(alertsDB))
	fmt.Println("Number of Canceled alerts in DB: ", alertCancelNum)
	fmt.Println("Number of Active alerts in DB: ", int(math.Abs((float64(len(alertsDB) - alertCancelNum)))))

	return alertsDB
}

func retrieveVROPSAlerts(vropsSession *api.VropsSession) []Alert {

	now := time.Now()
	milliEpoch := (now.Unix() * 1000) - ((5.0 * 60) * 1000)
	vropsAlertQuery := vrops.VropsAlertQuery{
		Xs:                       "http://www.w3.org/2001/XMLSchema",
		Xsi:                      "http://www.w3.org/2001/XMLSchema-instance",
		Ops:                      "http://webservice.vmware.com/vRealizeOpsMgr/1.0/",
		CompositeOperator:        "AND",
		ActiveOnly:               true,
		AlertCriticality:         "CRITICAL IMMEDIATE",
		IncludeChildrenResources: true,
		StartTimeRange: &vrops.VropsStartTimeRange{
			StartTime: 0,
			EndTime:   milliEpoch,
		},
	}
	var vropsAlertQueryResponse vrops.VropsAlertQueryResponse
	newAlerts, _ := retrieveAlerts(vropsSession, vropsAlertQuery, vropsAlertQueryResponse, false)
	fmt.Println("Number of Alerts retrieved from VROPs: ", len(newAlerts))

	return newAlerts
}

func main() {

	//Import our commandline flags
	vropsUrl := flag.String("vrops_url", "", "Url for the target vrops instance")
	vropsUsername := flag.String("vrops_username", "", "Username for the API service-account for vrops")
	vropsPassword := flag.String("vrops_password", "", "Password for the API service-account for vrops")
	dbHostName := flag.String("db_host", "", "Hostname for the DB to write to")
	dbName := flag.String("db_name", "", "Name of the database to write to")
	dbTable := flag.String("db_table", "", "Name of the database table to write to")
	dbUsername := flag.String("db_username", "", "Username to establish connection with DB")
	dbPassword := flag.String("db_password", "", "Password to establish connection with DB")

	flag.Parse()

    //Unfortunately the account provisioned for this has a `$` in its password
    //which prevents us from passing it in its original form via bash, so we need
    //to restore it
    password := strings.Replace(*vropsPassword, ":", "$", -1)
    
	//Establish our session, and defer the CloseSession handler to when main() gets off the stack
	vropsSession, err := api.NewSession("https://"+*vropsUrl, *vropsUsername, password)
	if err != nil {
		panic(err)
	}
    defer vropsSession.CloseSession("https://"+*vropsUrl, *vropsUsername, *vropsPassword)
    
	//Open our mysql db connection, and defer it's close
	db, err := sql.Open("mysql", *dbUsername+":"+*dbPassword+"@tcp("+*dbHostName+":3306)/"+"")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	//Grab all alerts whose monitoringSystem column equals "VROPS"
	rows, err := db.Query("SELECT alertId FROM " + *dbName + "." + *dbTable + " WHERE monitoringSystem='VROPS'")
	if err != nil {
		panic(err)
	}

	//Prepare our first VROPs alerts query, search for all alertIDs that we retrieved from the DB
	alertStringIds := ""
	for rows.Next() {
		var alertId string
		rows.Scan(&alertId)

		//Create a space separated string to hold our alertIDs
		alertStringIds += " " + string(alertId) + " "
	}

	//Issue query to retreive DB alerts

	var alertsDB []Alert
	if alertStringIds != "" {
		alertsDB = retrieveDBAlerts(vropsSession, alertStringIds)
	}

	//Prepare our second query for all new alerts in VROPS
	newAlerts := retrieveVROPSAlerts(vropsSession)

	//Compare to the two returned AlertSlices, and add only the new alerts that don't exist in our DB
	vropsAlerts := addToSliceIfNewAlert(alertsDB, newAlerts)
	for i := 0; i < len(vropsAlerts); i++ {

		//TODO: Need a better way of handling parameterized queries in golang
		query := "INSERT INTO " + *dbName + "." + *dbTable + " (entityName, entityType, entityID, alertId, alertStatus, alertSubject, alertMessage, alertUrl, alertType, alertStartTime, alertUpdateTime, alertCancelTime, alertCriticality, monitoringSystem)"
		values := " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE alertStatus = VALUES(alertStatus), alertUpdateTime = VALUES(alertUpdateTime), alertCancelTime = VALUES(alertCancelTime)"
		insert, err := db.Prepare(string(query + values))
		if err != nil {
			panic(err)
		}

		_, err = insert.Exec(vropsAlerts[i].EntityName,
			vropsAlerts[i].EntityType,
			vropsAlerts[i].EntityId,
			vropsAlerts[i].AlertId,
			vropsAlerts[i].AlertStatus,
			vropsAlerts[i].AlertSubject,
			vropsAlerts[i].AlertMessage,
			vropsAlerts[i].AlertURL,
			vropsAlerts[i].AlertType,
			vropsAlerts[i].AlertStartTime,
			vropsAlerts[i].AlertUpdateTime,
			vropsAlerts[i].AlertCancelTime,
			vropsAlerts[i].AlertCriticality,
			vropsAlerts[i].MonitoringSystem)

		if err != nil {
			insert.Close()
			panic(err)
		}
		insert.Close()
		
	}
}
